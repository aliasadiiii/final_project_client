import React, {Component} from 'react';

class Cell extends Component {
    render() {
        let width = '27px';
        let height = '27px';

        let first_visibility = this.props.first ? 'visible' : 'hidden';
        let second_visibility = this.props.second ? 'visible' : 'hidden';
        return (
            <div>
                <table>
                    <tbody>
                    <tr>
                        <td>
                            <img src={require('./static/first.png')}
                                 style={{width: width, height: height, visibility: first_visibility}}/>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <img src={require('./static/second.png')}
                                 style={{width: width, height: height, visibility: second_visibility}}/>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        )
    }
}

export default Cell;