import React, {Component} from 'react';
import Cell from './Cell';

let Parse = require('parse');

let n = 10;


class Board extends Component {
    constructor(props) {
        super(props);
        this.state = {
            score_first: 0,
            score_second: 0,
            turn: 'first',
            is_running: true,
        };

        this.onClick = this.onClick.bind(this);
        let query = new Parse.Query('Game');
        query.equalTo('objectId', this.props.game_id);
        this.subscription = query.subscribe();
    }

    componentDidMount() {
        this.subscription.on('update', (object) => {
            this.setState({
                score_first: object.attributes.score_first,
                score_second: object.attributes.score_second,
                turn: object.attributes.turn,
                is_running: Math.max(object.attributes.score_first, object.attributes.score_second) < 99,
            });
        });
    }

    onClick() {
        if (this.props.turn !== this.state.turn) {
            alert('It is not your turn.');
            return;
        }

        let query = new Parse.Query('Game');
        query.get(this.props.game_id, {
            success: (game) => {
                console.log(game);
                if (this.props.turn === 'first') {
                    if (game.get('second') === undefined) {
                        let nextStep1 = step(this.state.score_first);
                        let nextStep2 = step(this.state.score_second);
                        game.set('score_first', nextStep1);
                        game.set('score_second', nextStep2);
                        game.set('is_running', Math.max(nextStep1, nextStep2) < 99);
                        game.set('turn', 'first');
                        game.save();

                    } else {
                        let nextStep = step(this.state.score_first);
                        game.set('score_first', nextStep);
                        game.set('is_running', nextStep < 99);
                        game.set('turn', 'second');
                        game.save();
                    }
                } else {
                    let nextStep = step(this.state.score_second);
                    game.set('score_second', nextStep);
                    game.set('is_running', nextStep < 99);
                    game.set('turn', 'first');
                    game.save();
                }
            },
            error: (obj, err) => {
                console.log(err);
            }
        });
    }

    render() {
        if (!this.state.is_running) {
            return (
                <div style={{margin: '0 auto', width: '300px', marginTop: '50px'}}>
                    The game is over <br/>
                    <a href={'/'}>Back To Homepage</a>
                </div>
            )
        }

        let len = '600px';
        let rows = [];
        for (let i = 0; i < n; i++) {
            let cells = [];
            for (let j = 0; j < n; j++) {
                let first = get_score(i, j) === this.state.score_first;
                let second = get_score(i, j) === this.state.score_second;
                cells.push(<td key={j}><Cell first={first} second={second}/></td>);
            }
            rows.push(<tr key={i}>{cells}</tr>);
        }
        return (
            <div>
                <div style={{
                    margin: '0 auto',
                    backgroundSize: '600px 600px',
                    backgroundImage: 'url("' + require('./static/grid.jpg') + '")', width: len, height: len,
                }}>
                    <table>
                        <tbody>
                        {rows}
                        </tbody>
                    </table>
                </div>
                <button onClick={this.onClick} style={{width: '60px', height: '60px'}}>toss</button>
            </div>
        );
    }
}

function get_score(x, y) {
    return (n - x - 1) * n + y;
}

function get_real_score(x) {
    let map = new Map();
    map.set(2, 50);
    map.set(33, 0);
    map.set(24, 4);
    map.set(5, 26);
    map.set(46, 18);
    map.set(19, 69);
    map.set(35, 54);
    map.set(64, 51);
    map.set(86, 56);
    map.set(67, 97);
    map.set(98, 68);
    map.set(62, 94);
    map.set(90, 60);

    if (map.has(x))
        return map.get(x);
    return x;
}

function step(x) {
    let nextX = x + Math.ceil(6 * Math.random());
    if (nextX >= 100)
        return x;
    else
        return get_real_score(nextX);
}

export default Board;