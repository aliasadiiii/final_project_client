import React, {Component} from 'react';
import {loggedIn} from "./utils";
import Board from "./Board";

let Parse = require('parse');
Parse.initialize('myAppId', '1xoWtDkxw8oZvX3bzhdTuHU7KZB8SGZD9jWQ2V9p', 'myMasterKey');
Parse.serverURL = 'http://localhost:1337/parse';
Parse.User.enableUnsafeCurrentUser();

class Game extends Component {
    constructor(props) {
        super(props);
        this.onClick = this.onClick.bind(this);
        Game.logOut = Game.logOut.bind(this);
        this.state = {
            turn: undefined,
            game_id: undefined,
            score: 0,
        }
    }

    static logOut() {
        Parse.User.logOut();
        setTimeout(() => {
            window.location.href = '/'
        }, 300)
    }

    onClick() {
        Parse.Cloud.run('new_game').then((resp) => {
            this.setState({
                turn: resp.turn,
                game_id: resp.game_id
            });
        });
    }

    componentDidMount() {
        let user = Parse.User.current();
        user.fetch().then((u) => {
            let score = u.get('score');
            this.setState({
                score: score
            });
        });
    }

    render() {
        if (!loggedIn()) {
            window.location.href = "/signin";
        }

        if (this.state.game_id !== undefined && this.state.turn !== undefined) {
            return (
                <Board game_id={this.state.game_id} turn={this.state.turn}/>
            )
        } else {
            return (
                <div style={{margin: '0 auto', width: '300px', marginTop: '50px'}}>
                    <h3>Score:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {this.state.score}</h3>
                    <button className='btn btn-primary' style={{marginRight:'30px'}} onClick={this.onClick}>New game</button>
                    <button className='btn btn-info' onClick={Game.logOut}>Log Out</button>
                    <br/> <br/>
                    <a href={'/scoreboard'} style={{marginRight:'75px'}}>Scoreboard</a>
                    <a href={'/edit'}>Edit Profile</a>
                </div>
            )
        }
    }
}

export default Game;