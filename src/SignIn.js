import React, {Component} from 'react';

let Parse = require('parse');

class SignIn extends Component {
    constructor(props) {
        super(props);
        this.onClick = this.onClick.bind(this);
    }

    onClick() {
        Parse.User.logIn(this.username.value, this.password.value, {
            success: function (user) {
                console.log('user logged in');
            },
            error: function (user, error) {
                console.log('error while logging %s' % error)
            }
        });


        setTimeout(() => {
            window.location.href = '/'
        }, 300);
    }

    render() {
        return (
            <div style={{margin: '0 auto', width: '200px', marginTop: '50px'}}>
                <form method={'post'}>
                    Username: <br/>
                    <input type={'text'} name={'username'} ref={(input) => {
                        this.username = input
                    }}/> <br/>
                    Password: <br/>
                    <input type={'password'} name={'password'} ref={(input) => {
                        this.password = input
                    }}/> <br/>
                </form>
                <br/>
                <button onClick={this.onClick} className={'btn btn-success'} style={{marginRight: '20px'}}>Sign In</button>
                <a href={'/signup'}>Sign Up</a>
            </div>
        )
    }
}

export default SignIn;
