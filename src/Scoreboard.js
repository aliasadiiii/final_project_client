import React, {Component} from 'react';

let Parse = require('parse');

class Scoreboard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            users: []
        }
    }

    componentDidMount() {
        let query = new Parse.Query(Parse.User);
        if (this.props.level === 'country') {
            let user = Parse.User.current();
            user.fetch().then((u) => {
                query.equalTo('nationality', u.get('nationality'));
                query.descending("score");
                query.limit(20);
                query.find({
                    success: (results) => {
                        this.setState({
                            users: results
                        });
                    },
                    error: (error) => {
                        console.log(error);
                    }
                });
            });
        } else {
            query.descending("score");
            query.limit(20);
            query.find({
                success: (results) => {
                    this.setState({
                        users: results
                    });
                },
                error: (error) => {
                    console.log(error);
                }
            });
        }
    }

    render() {
        let rows = [];
        for (let i = 0; i < this.state.users.length; i++) {
            let user = this.state.users[i];
            let name = user.get('first_name') + ' ' + user.get('last_name');
            let score = user.get('score');
            let nationality = user.get('nationality');
            rows.push((<tr key={i}>
                <td>{name}</td>
                <td>
                    <div style={{width: '5px', margin: '0 auto'}}>{score}</div>
                </td>
                <td>{nationality}</td>
            </tr>));
        }
        return (
            <div style={{margin: '0 auto', width: '200px', marginTop: '50px'}}>
                <table className="table table-striped">
                    <thead>
                    <tr>
                        <th><a href={'/scoreboard/global'}>Global</a></th>
                        <th>
                            <div style={{width: '50px'}}/>
                        </th>
                        <th><a href={'/scoreboard/country'}>Country</a></th>
                    </tr>
                    </thead>
                    <tbody>
                    {rows}
                    </tbody>
                </table>
                <a href={'/'}>Homepage</a>
            </div>
        );
    }
}


export const GlobalScoreboard = function () {
    return (
        <Scoreboard level={'global'}/>
    )
};

export const CountryScoreboard = function () {
    return (
        <Scoreboard level={'country'}/>
    )

};




