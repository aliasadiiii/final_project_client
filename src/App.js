import React, {Component} from 'react';
import './App.css';
import SignIn from "./SignIn";
import SignUp from './SignUp';
import Game from './Game';
import {CountryScoreboard, GlobalScoreboard} from './Scoreboard';
import EditProfile from "./EditProfile";
import {
    Route,
    BrowserRouter as Router
} from 'react-router-dom';


class App extends Component {
    render() {
        return (
            <Router>
                <div>
                    <Route exact path='/' component={Game}/>
                    <Route path='/signin' component={SignIn}/>
                    <Route path='/signup' component={SignUp}/>
                    <Route exact path='/scoreboard' component={GlobalScoreboard}/>
                    <Route path='/scoreboard/global' component={GlobalScoreboard}/>
                    <Route path='/scoreboard/country' component={CountryScoreboard}/>
                    <Route path='/edit' component={EditProfile}/>

                </div>
            </Router>
        );
    }
}

export default App;
