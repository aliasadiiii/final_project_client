import React, {Component} from 'react';

let Parse = require('parse');

class EditProfile extends Component {
    constructor(props) {
        super(props);
        this.onClick = this.onClick.bind(this);
    }

    onClick() {
        let user = new Parse.User.current();
        user.set("first_name", this.first_name.value);
        user.set("last_name", this.last_name.value);
        user.set("nationality", this.nationality.value);
        user.set("gender", this.gender.value);

        user.save(null, {
            success: function (user) {
                console.log('user edited profile');
            },
            error: function (user, error) {
                console.log('Error while signing up %s' % error);
            }
        });
        setTimeout(() => {
            window.location.href = '/'
        }, 300);
    }

    render() {
        return (
            <div style={{margin: '0 auto', width: '200px', marginTop: '50px'}}>
                <form method={'post'}>
                    First Name: <br/>
                    <input type={'text'} name={'first_name'} ref={(input) => {
                        this.first_name = input;
                    }}/> <br/>
                    Last Name: <br/>
                    <input type={'text'} name={'last_name'} ref={(input) => {
                        this.last_name = input;
                    }}/> <br/>
                    Nationality: <br/>
                    <input type={'text'} name={'nationality'} ref={(input) => {
                        this.nationality = input;
                    }}/> <br/>
                    Gender: <br/>
                    <input type={'text'} name={'gender'} ref={(input) => {
                        this.gender = input;
                    }}/> <br/>
                </form>
                <br/>
                <button className={'btn btn-success'} onClick={this.onClick} style={{width:'50px', marginRight: '55px'}}>Edit</button>
                <a href={'/'}>Homepage</a>
            </div>
        )

    }
}

export default EditProfile;