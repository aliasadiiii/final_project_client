let Parse = require('parse');

export function loggedIn() {
    return !!Parse.User.current();
}

