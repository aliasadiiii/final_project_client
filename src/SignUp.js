import React, {Component} from 'react';

let Parse = require('parse');

class SignUp extends Component {
    constructor(props) {
        super(props);
        this.onClick = this.onClick.bind(this);
    }

    onClick() {
        let user = new Parse.User();
        user.set("username", this.username.value);
        user.set("password", this.password.value);
        user.set("first_name", this.first_name.value);
        user.set("last_name", this.last_name.value);
        user.set("nationality", this.nationality.value);
        user.set("gender", this.gender.value);
        user.set("score", 0);

        user.signUp(null, {
            success: function (user) {
                console.log('user signed up');
            },
            error: function (user, error) {
                console.log('Error while signing up %s' % error);
            }
        });
        setTimeout(() => {
            window.location.href = '/'
        }, 300);
    }

    render() {
        return (
            <div style={{margin: '0 auto', width: '200px', marginTop: '50px'}}>
                <form method={'post'}>
                    Username: <br/>
                    <input type={'text'} name={'username'} ref={(input) => {
                        this.username = input;
                    }}/> <br/>
                    First Name: <br/>
                    <input type={'text'} name={'first_name'} ref={(input) => {
                        this.first_name = input;
                    }}/> <br/>
                    Last Name: <br/>
                    <input type={'text'} name={'last_name'} ref={(input) => {
                        this.last_name = input;
                    }}/> <br/>
                    Nationality: <br/>
                    <input type={'text'} name={'nationality'} ref={(input) => {
                        this.nationality = input;
                    }}/> <br/>
                    Gender: <br/>
                    <input type={'text'} name={'gender'} ref={(input) => {
                        this.gender = input;
                    }}/> <br/>
                    Password: <br/>
                    <input type={'password'} name={'password'} ref={(input) => {
                        this.password = input;
                    }}/> <br/>
                </form>
                <br/>
                <button className={'btn btn-success'} onClick={this.onClick} style={{marginRight: '20px'}}>Sign Up</button>
                <a href={'/signin'}>Sign In</a>
            </div>
        )

    }
}


export default SignUp;